import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.Collection;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(Parameterized.class)
public class VerifyIfAccountIsCreatedWithValidInputs {
    public static final Integer TIMEOUT_IN_SECONDS = 15;
    public static WebDriver driver;
    public static WebDriverWait waiter;

    private String fName;
    private String lName;
    private String mobileEmail;
    private String pass;

    public VerifyIfAccountIsCreatedWithValidInputs(String fName, String lName, String mobileEmail, String pass) {
        this.fName = fName;
        this.lName = lName;
        this.mobileEmail = mobileEmail;
        this.pass = pass;
    }

    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {"Mary", "JaneGoolga", "0756789344", "howdy123"}, {"Ana", "DelRay", "0756389344", "howdy456"}
                , {"AnaAnita", "DelRays", "0756349344", "abcde@f"}, {"ddAna", "DelRdsay", "0736389344", "nicepassw"}
        };
        return Arrays.asList(data);
    }

    @BeforeClass
    public static void openChrome() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        waiter = new WebDriverWait(driver, TIMEOUT_IN_SECONDS);
        System.out.println("BEFORECLASS");
    }

    @AfterClass
    public static void closeChrome() {
        driver.close();
        System.out.println("afterCLASS");
    }

    @Before
    public void openFB() {
        driver.get("https://www.facebook.com/");
        waiter.until(ExpectedConditions.titleIs("Facebook - Log In or Sign Up"));
        System.out.println("BEFORE");
    }

    @After
    public void closeFB() {
        driver.manage().deleteAllCookies();
        driver.navigate().to("https://www.google.com/?gws_rd=ssl");
        System.out.println("after");
    }

    @Test
    public void testSeveralInputsInSignUp() {
        Actions actions = new Actions(driver);
        insertStringInputs(actions);
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_s\"]/span[1]"))).click().perform();
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_u\"]"))).click().perform();
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"u_0_5\"]")));
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"facebook\"]/body/div[3]/div[2]/div/div/div/div[3]/button"))).click().perform();
        waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/div/div/div[1]/div[2]/div")));
    }

    private void insertStringInputs(Actions actions) {
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_b\"]/div[1]"))).click().sendKeys(fName).perform();
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_d\"]/div[1]"))).click().sendKeys(lName).perform();
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_g\"]/div[1]"))).click().sendKeys(mobileEmail).perform();
        actions.moveToElement(driver.findElement(By.xpath("//*[@id=\"u_0_n\"]/div[1]"))).click().sendKeys(pass).perform();
    }
}
